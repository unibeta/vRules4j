package com.unibeta.vrules.engines.dccimpls;

import com.unibeta.vrules.engines.dccimpls.core.MemoryInnerCoreEngineImpl;
import com.unibeta.vrules.engines.dccimpls.core.ExternalStreamedCoreEngineImpl;
import com.unibeta.vrules.tools.CommonSyntaxs;

public class CoreEngineFactory {

	public static CoreEngine getInstance() {
		if(CommonSyntaxs.getEnableMemoryInnerJavaCompiler()) {
			return new MemoryInnerCoreEngineImpl();
		}else {
			return new ExternalStreamedCoreEngineImpl();
		}
		
	}
}
