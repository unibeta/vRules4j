package com.unibeta.vrules.engines.dccimpls;

public interface CoreEngine {

	public String[] validate(Object object, String fileName, String entityId, Object decisionObject, String vrulesMode)
			throws Exception ;
}
