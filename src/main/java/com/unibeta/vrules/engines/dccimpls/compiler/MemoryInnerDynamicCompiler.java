package com.unibeta.vrules.engines.dccimpls.compiler;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unibeta.vrules.engines.dccimpls.compiler.memory.MemoryJavaCompiler;
import com.unibeta.vrules.engines.dccimpls.compiler.memory.SpringBootMemoryClassLoader;
import com.unibeta.vrules.servlets.URLConfiguration;
import com.unibeta.vrules.tools.CommonSyntaxs;
import com.unibeta.vrules.utils.CommonUtils;

public class MemoryInnerDynamicCompiler extends MemoryJavaCompiler {
	private static Logger log = LoggerFactory.getLogger(ExternalStreamedValidationClassLoader.class);

	@Override
	public Map<String, byte[]> compile(String fileName, String source) throws Exception {

		if (!CommonUtils.isNullOrEmpty(CommonSyntaxs.getXspringbootJarPath())) {
			try {
				Class.forName("com.sun.tools.javac.file.JavacFileManager");
				Class.forName("org.springframework.boot.loader.jar.JarFile");
				Class.forName("org.springframework.boot.loader.LaunchedURLClassLoader");
				Class.forName("org.springframework.boot.loader.JarLauncher");

				return SpringBootMemoryClassLoader.compile(fileName, source);
			} catch (ClassNotFoundException e) {
				log.debug(SpringBootMemoryClassLoader.class.getCanonicalName() + " compile failed, caused by "
						+ e.getMessage());
				log.debug("try use " + MemoryJavaCompiler.class.getCanonicalName() + " to compile...");

				String bootJarClassPath = URLConfiguration.resolveSpringBootJarClasspath();
				if (!URLConfiguration.getClasspath().contains(bootJarClassPath)) {
					URLConfiguration
							.setClasspath(URLConfiguration.getClasspath() + File.pathSeparator + bootJarClassPath);
				}

				return super.compile(fileName + ".java", source);
			}
		} else {
			return super.compile(fileName + ".java", source);
		}
	}

	public static void main(String[] args) {

		String fileName = "D:\\Projects\\vRules4j_gitee\\vRules4j\\target\\DEclipseVRules3XDoc1FatherRules2.java";
		String className = ExternalStreamedValidationClassLoader.COM_UNIBETA_VRULES_ENGINES_DCCIMPLS_RULES
				+ CommonUtils.getFileSimpleName(fileName);

		try {
			String javeSrc = new String(Files.readAllBytes(Paths.get(fileName)));

			MemoryJavaCompiler compiler = new MemoryJavaCompiler();
			Map<String, byte[]> results = compiler.compile("DEclipseVRules3XDoc1FatherRules2.java", javeSrc);

			// compile
//		      GroovyClassLoader gcl = new GroovyClassLoader(Thread.currentThread().getContextClassLoader());
//			Class<?> classLoad = gcl.parseClass(javeSrc , className + ".groovy");

//			MemoryOnlyValidationClassLoader classLoad = new MemoryOnlyValidationClassLoader(className,MemoryOnlyDynamicCompiler.compile(fileName),null);
			CommonSyntaxs.println("result:" + compiler.loadClass(className, results));

//			Thread.currentThread().getContextClassLoader().loadClass(className);
//			Class.forName(className);

//			Object obj = new ValidationClassLoader(fileName, null).newValidationInstance();
//			CommonSyntaxs.println(obj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}