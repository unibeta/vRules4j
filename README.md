# vRules4j
vRules4j--A Powerful Object Oriented Java Object Validation/Decision Rule engine,which is born for taking over all validation responsibility in java business-enterprise industry and looking forward to providing Perfect/Lightweight solution.

#### 项目介绍
vRules4j是由个人发起的开源项目，它基于Apache Licenses 2.0 免费发布。任何组织或者个人都可以查看其源码并根据自己的需要修改源码，同时在商业应用中具有最大的授权许可。具体Licenses 条款请参阅http://www.apache.org/licenses/

vRules4j是免费的软件，发布的初衷是希望他对开发者之外的组织或者个人同样有应用价值，但是如果由于软件本身存在的未知的潜在的安全隐患而导致的任何后果，原作者或者开发组织将不承担任何责任。

vRules4j是一款短小精悍,超级灵活,扩展性极强的规则引擎,底层没有依赖第三方规则引擎,发布包仅有100多K.尤其擅长对符合Java Bean规范的POJO对象进行多维度规则定义并且根据用户定义的对象结构输出验证结果.

该开源产品已经在多个项目的生产环境稳定运行, 在功能完整性,扩展性,灵活性,数据安全/一致性等领域经受了考验.

#### 功能介绍
vRules4j是一款应用于java对象分析校验的规则引擎。通过外部规则定义即可实现程序运行时动态的校验应用对象功能，并根据用户定义的错误提示信息进行友好提示。

可以采用该引擎校验的目标对象可以为用户自定义的所有继承于Object类型的java对象以及java原始数据类型的对象（如int，float，double，boolean等等），另外包含Map和List所存放的复杂对象，并可以递归的分别对每个元素值进行深度分析校验。

该引擎可应用于具有复杂的校验规则而且要求灵活可配置的需求场景。它解决的关键问题是:输入要验证的模型对象根据规则定义返回可定义的分析验证结果模型.一般步骤如下:
- 构建验证模型,通常是Java POJO.
- 调用类似com.unibeta.vrules.engines.ValidationEngineFactory.getInstance().validate(java.lang.Object object, java.lang.String vRulesFileName)即可返回String[]类型验证结果.如果想返回自定义的结果模型,请参考最后代码样例.
- 获得验证结果后做其他数据加工处理.

#### 引擎优势
- 100%支持原生java语法,运行高效.与本地class速度一致.
- 配置规则可读性可维护性可扩展性高.
- 功能完整而且强大.
- 支持任意复杂度的深度递归分析
- 支持面向对象的规则定义
- 支持对象继承关系校验
- 支持多个属性的关联校验
- 可以调用用户自定义的业务校验方法.
- XPath准确定位错误发生的位置
- 校验条件的谓词支持标准java语言语法.
- 规则文件可以组织重用.(includes功能)
- 内置vRules4j-Digester工具帮助生成相应的对象校验规则文件.
- 应用简单,与本地程序耦合度极低.
- 用户可注册定制自己想要的校验失败后返回的DecisionObject.
- 可以与web容器灵活集成,将vRules4j应用到企业应用中去.
- 支持i18N.

#### 代码样例

下面代码样例演示了常用的多对象验证的调用方法, 其中examples.cases.ErrorInfo被指定为分析结果断言模型.需要使用@XmlRootElement做注解声明.

###### 特别说明:
- 规则描述xml文件具有固定格式,它能支持对任意深层对象的任意属性定义专有规则并指定他们的依赖关系和执行顺序, 对于大对象而言人工维护配置将会是巨大的工作量. 
vRules4j已经考虑到这点并实现了基于java对象自动生成xml规则描述文件的功能.如果是第一次运行, 指定的fileName不存在,引擎会根据输入的Java对象自动生成一个符合对象结构关系的xml规则描述文件,用户只需要在里面找到自己需要定义规则的属性并使用java语法定义规则即可.
- ```<imports></imports>```用于导入外部服务类
- ```<java></java>```用于定义全局变量或者方法
- ```<global>```里的```<includes></includes>```用于导入外部规则文件实现重用. 例如:```<includes>../common.xml</includes>```导入当前文件上一级目录下的common.xml

```java
@XmlRootElement  
public class ErrorInfo{  
    private String errorCode; //用户字段,可以在xml规则文件里配置  
    @ValidationErrorMessage //验证注解字段,验证信息自动赋值到errorMsg字段   
    String errorMsg;   
    @ValidationErrorXPath //验证注解字段, 验证失败字段的xpath自动赋值到xPath字段   
    String xPath;   
    ErrorField[]  errorField; //用户字段,可以在xml规则文件里配置    
    ErrorField error; //用户字段,可以在xml规则文件里配置   
}  

public static void main(String[] args) {

        BasicConfigurator.configure();
        String fileName = "D:/eclipse/vRuleExample-3.0/rules/exampleRules.xml";

        String errs[] = null;
        List<examples.cases.ErrorInfo> list = null;
        
        try {
            buildFamily();
            ValidationEngine engine = ValidationEngineFactory.getInstance();

            //list = engine.validate(new Object[] { family }, fileName,info);
            //Java2vRules.digest(new Class[]{ISon.class}, fileName,info); //基于java对象自动生成规则描述xml文件到fileName, 并注册输出验证结果模型为info对象.

            Map<String,Object> map = new HashMap();
            map.put("ISon", son);
            map.put("Family", family);

            long start = System.currentTimeMillis();
            
            /*
            	接受Object数组并基于fileName规则文件进行验证,list是验证失败的对象列表(数据类型是info)
            */
            list = engine.validate(new Object[] {  son, family, father  }, fileName, info);

            /*
            	接受Object数组并基于fileName规则文件进行验证,默认返回String类型的验证结果
            */
            errs = engine.validate(new Object[] {  family,son, family, father }, fileName);       
            long end = System.currentTimeMillis();

            System.out.println("the cost is " + (end - start) / 1000.00);
            
            errs = engine.validate( map ,fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null != list) {
            System.out.println("error is " + list.size());
            for (ErrorInfo e : list) {
                System.out.println("" + e.getXPath() +":" + e.getErrorCode());
                System.out.println("filed value is " + e.getError().getErrorId());
            }

        }

        if (null != errs) {
            System.out.println("Error messages are:" + errs.length);
            for (int i = 0; i < errs.length; i++) {
                // System.out.println(errs[i].substring(errs[i].indexOf("${xPath}:=")+10));
                System.out.println(errs[i]);
            }
        }

        System.out.println("End");
    }
```
#### API说明
###### ValidationEngine
```java
Interface com.unibeta.vrules.engines.ValidationEngine
public interface ValidationEngine
ValidationEngine interface. The validation engine service interface.

 java.lang.String[]	validate(java.util.Map<java.lang.String,java.lang.Object> objMap, java.lang.String vRulesFileName)
          Validates the objects in Map and return error messages if validates failed, the key of the map element can be the validation entityId configured in validation files.
 java.util.List	validate(java.util.Map<java.lang.String,java.lang.Object> objMap, java.lang.String vRulesFileName, java.lang.Object decisionObj)
          Validates the objects in Map, the key of the map element can be the validation entityId configured in validation files.
 java.lang.String[]	validate(java.lang.Object[] objects, java.lang.String vRulesFileName)
          Validates a array of objects via the specified rules' file.The input parameter object can be whatever datatype.
 java.util.List	validate(java.lang.Object[] objects, java.lang.String vRulesFileName, java.lang.Object decisionObj)
          Validates an array object via the specified rule's file, meanwhile registering the returned error object instance type.
 java.lang.String[]	validate(java.lang.Object object, java.lang.String vRulesFileName)
          Validates the specified object via the validation rules.
 java.util.List	validate(java.lang.Object object, java.lang.String vRulesFileName, java.lang.Object decisionObj)
          Validates an object via the specified rule's file, meanwhile registering the returned error object instance type.
 java.lang.String[]	validate(java.lang.Object object, java.lang.String vRulesFileName, java.lang.String entityId)
          Validates the specified object by entityId.The paramater object can be whatever datatype.
 java.util.List	validate(java.lang.Object object, java.lang.String vRulesFileName, java.lang.String entityId, java.lang.Object decisionObj)
 ```
 ###### VRules4jServlet
com.unibeta.vrules.servlets.VRules4jServlet 
-  应用于web容器时, 需要配置该Servlet用于自动初始化classpth实现和容器管理的ClassLoader进行无缝集成,
- 也可以使用'-DvRules4j.classpath='在JVM启动参数里配置外部classpath

#### 规则文件
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 
@gt > 
@lt < 
@lteq <= 
@gteq >= 
@or || 
@and && 
@bitwise_and & 
@bitwise_or | 
@left_shift << 
@right_shift >> 
@right_unsigned_shift >>> 
@and_assign &= 
@or_assign |= 
@left_shift_assign <<= 
@right_shift_assign >>= 
@right_unsigned_shift_assign >>>= 

$Powered By vRules4j-Digester Automatically On Fri Sep 17 18:24:32 CST 2021$
Visit http://sourceforge.net/projects/vrules/ to download the latest version

Author: jordan.xue
-->
<vRules4j>
    <global>
        <decisionMode>false</decisionMode>
        <assertion>false</assertion>
        <returnErrorId>false</returnErrorId>
        <displayErrorValue>true</displayErrorValue>
        <toggleBreakpoint>off</toggleBreakpoint>
        <enableBinding>false</enableBinding>
        <mergeExtensions>false</mergeExtensions>
        <includes/>
    </global>
    <imports>static com.unibeta.cloudtest.util.ObjectDigester.*;</imports>
    <java>
		com.unibeta.cloudtest.config.CacheManager $cache$ = com.unibeta.cloudtest.config.CacheManagerFactory.getInstance();
	</java>
    <contexts>
        <context
            className="com.unibeta.cloudtest.openapi.OpenAPIHub.Response" name="ResponseResult"/>
        <context
            className="com.unibeta.cloudtest.openapi.OpenAPIHub.Response" name="$Response"/>
        <context className="com.unibeta.cloudtest.util.ObjectDigester" name="$CloudObject$"/>
    </contexts>
    <decisionConstants>
        <definition name="" value=""/>
    </decisionConstants>
    <ruleset desc="" id="">
        <rule desc="" id=""/>
    </ruleset>
    <object desc="" extensions=""
        id="loadLatestReserveApproveHistoryUsingGET" name="loadLatestReserveApproveHistoryUsingGET">
        <className>com.unibeta.cloudtest.assertion.CloudTestAssert</className>
        <nillable>false</nillable>
        <rules>
            <rule breakpoint="off" depends="" desc=""
                id="loadLatestReserveApproveHistoryUsingGETRule"
                ifFalse="" ifTrue="" isComplexType="false"
                isMapOrList="false"
                name="loadLatestReserveApproveHistoryUsingGETRule"
                recursive="" refId="" sequence="">
                <binding foreach="" var="" wirings=""/>
                <dataType/>
                <attributes/>
                <boolParam>$</boolParam>
                <assert>
                if($Response.getStatus() == 200) {
                	//if($Response.getBody() instanceof Map) {Map body = (Map)$Response.getBody();}
                	$ = true;
                }else{
                	$ = false;
                }
                </assert>
                <message id="loadLatestReserveApproveHistoryUsingGET" returnId="false">Assert failed, response value is:#{$CloudObject$.toJSON($Response)}</message>
                <decision/>
            </rule>
        </rules>
    </object>
  </vRules4j>

```
